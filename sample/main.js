// For any third party dependencies, like jQuery, place them in the lib folder.

// Configure loading modules from the lib directory,
// except for 'app' ones, which are in a sibling
// directory.
requirejs.config({
    baseUrl: '../node_modules',
    paths: {
        'mp_geometry':'../dist/local/client',
        'sample': '../sample/sample',
        'jquery':'jquery/dist/jquery.min',
        'lodash':'lodash/lodash',
        'bootstrap.bundle.min':'bootstrap/dist/js/bootstrap.bundle.min',
    },
    shim: {
        'mp_geometry' :                 { deps: ['bootstrap.bundle.min','jquery','lodash']},
        'sample' :                     { deps: ['mp_geometry']}
    }
});

// Start loading the main app file. Put all of
// your application logic in there.
require(['sample']);