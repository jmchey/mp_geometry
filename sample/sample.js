define([], function () {
    console.log("Loading sample 1");
    var myPallete = $("#sample").Palette({
        layers: [
            {
                "id":"layer1",
                "shapes": [
                    new Tools.ShapeCircularCtrlUI(new Geometry.Circle(new Geometry.Point(100,500), 40)),
                    new Tools.ShapeLinearCtrlUI(new Geometry.Polygon([
                        new Geometry.Point(150,300),
                        new Geometry.Point(250,150),
                        new Geometry.Point(75,25)
                    ]))
                ]
            },
            {"id":"layer2"}
        ],
        toolbox: true
    });
});