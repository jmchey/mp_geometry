/// <reference path="./Vector.ts" />

namespace Geometry {

	export class Point extends Shape{

		static readonly className = "Point";
		private x: number;
		private y: number;

		constructor(_x: number, _y: number){
			super("POINT");
			this.x = _x;
			this.y = _y;
		}

		getType(): string{
			return Point.className;
		}

		getX(): number{
			return this.x;
		}

		setX(_x:number): void{
			this.x = _x;
			this.notifyChange("X");
		}

		getY(): number{
			return this.y;
		}

		setY(_y:number): void{
			this.y = _y;
			this.notifyChange("Y");
		}

		update(_timestamp: number): Vector{
			let v: Vector = super.update(_timestamp);
			this.move(v);
			return v;
		}

		move(v:Vector): void{
			this.x += v.getDx();
			this.y += v.getDy();
			this.notifyChange("XY");
		}

	}
}