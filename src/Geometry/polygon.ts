/// <reference path="./Shape.ts" />
/// <reference path="./Point.ts" />

module Geometry {

	export class Polygon extends Shape{
		static readonly className = "Polygon";
		private points: Point[];

		constructor(_points?: Point[], _color?: string) {
			super("POLYGON", _color);
			this.points=(_points!=null)?_points:[];
		};

		getType(): string{
			return Polygon.className;
		}

		getPoints(): Point[]{
			return this.points;
		};

		setPoints(_points: Point[]): void{
			this.points = _points;
			this.notifyChange();
		};

		addPoint(_point: Point){
			this.points.push(_point);
			this.notifyChange();
		}

		update(_timestamp: number): Vector{
			let v: Vector = super.update(_timestamp);
			for(let p of this.points){
				p.move(v);
			}
			return v;
		}
	}

}

