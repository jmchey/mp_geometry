namespace Geometry {

	export interface ShapeInterface{
		getId(): any;
		getType(): string;
		setToolbox(toolbox : JQuery) : void;
        getToolbox() : JQuery;
        setSelected(selected :boolean): void;
        isSelected(): boolean;
        setParentLayer(parentLayer: Tools.LayerUI): void;
        getParentLayer(): Tools.LayerUI;
        getName(): string;
        setName(_name: string): void;
        getColor(): string;
        setColor(_color: string): void;
        getSpin(): number;
        setSpin(_spin: number): void;
        getDirection(): number;
        setDirection(_direction: number): void;
        getSpeed(): number;
        setSpeed(_speed: number): void;
	}
}