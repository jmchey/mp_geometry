/// <reference path="./Shape.ts" />

namespace Geometry {
	export class Layer{

		protected id: any;
		protected shapes : Geometry.Shape[] = [];

		constructor(options?: any){
			this.id = (options != undefined && options.id)?options.id: new Date().getMilliseconds;
		}
		
		//Getter & setter

		setId(_id: string){
			this.id = _id;
		}

		getId(): string{
			return this.id;
		}

		getShapes(): Geometry.Shape[]{
			return this.shapes;
		}

		setShapes(_shapes: Geometry.Shape[]): void{
			this.shapes = _shapes;
		}

		addShape(_shape: Geometry.Shape): void{
			this.shapes.push(_shape);
		}

		removeShape(_shape: Geometry.Shape): void{

		}

		removeAllShape(): void{
			this.shapes = [];
		}

	}
}