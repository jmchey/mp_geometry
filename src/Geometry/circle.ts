/// <reference path="./Vector.ts" />
/// <reference path="./Shape.ts" />
/// <reference path="./Point.ts" />

namespace Geometry {

	export class Circle extends Shape {

		static readonly className = "Circle";
		private radius: number;
		private center: Point;

		constructor(_center?:Point, _radius?:number){
			super("CIRCLE");
			this.center=_center;
			this.radius=_radius;

		}

		getType(): string{
			return Circle.className;
		}

		getCenter(): Point{
			return this.center;
		}

		setCenter(_center: Point): void{
			this.center = _center;
			this.center.notifyChange("center");
		}

		getRadius(): number{
			return this.radius;
			this.notifyChange("radius");
		}

		setRadius(_radius: number): void{
			this.radius = _radius;
			this.notifyChange("radius");
		}

		update(_timestamp: number): Vector{
			let v: Vector = super.update(_timestamp);
			this.center.move(v);
			return v;
		}
	}
}