/// <reference path="./Vector.ts" />
/// <reference path="./ShapeInterface.ts" />

namespace Geometry {

    export abstract class Shape implements ShapeInterface{
        readonly id: any;
        private name: string;
        private color: string;
        private selected: boolean = false;
        private parentLayer: Tools.LayerUI;

        //Toolbox control panel
        private toolbox : JQuery;

        //Animation param
        private spin: number = 0.00;
        private direction: number = 0;
        private speed: number = 0.1;

        //Generate unique ID to identify shapes 
        private static generateId = (() => {
            var private_count = 0;
            var increment = () => {
                private_count += 1;
                return private_count;
            };
        
            return increment;
        })();

        

        constructor(_name: string, _color = "#aaa") {
            this.id = Shape.generateId(); 
            this.name = _name;
            this.color = _color
        };

        abstract getType(): string;
        

        /** Getter & setter **/
        getId(): any{
            return this.id;
        };

        setToolbox(toolbox : JQuery) : void{
            this.toolbox = toolbox;
        }

        getToolbox() : JQuery{
            return this.toolbox ;
        }

        setSelected(selected :boolean){
            this.selected = selected;
            this.notifyChange("selected");
        }

        isSelected(): boolean{
            return this.selected;
        }

        getParentLayer(): Tools.LayerUI{
            return this.parentLayer;
        }

        setParentLayer(parentLayer: Tools.LayerUI): void{
            this.parentLayer = parentLayer;
        }

        getName(): string{
            return this.name;
        };

        setName(_name: string): void{
            this.name = _name;
            this.notifyChange("name");
        };

        getColor(): string{
            return this.color;
        };

        setColor(_color: string): void{
            this.color = _color;
            this.notifyChange("color");
        };

        getSpin(): number{
            return this.spin;
        };

        setSpin(_spin: number): void{
            this.spin = _spin;
            this.notifyChange("spin");
        };

        getDirection(): number{
            return this.direction;
        };

        setDirection(_direction: number): void{
            this.direction = _direction;
            this.notifyChange("direction");
        };

        getSpeed(): number{
            return this.speed;
        };

        setSpeed(_speed: number): void{
            this.speed = _speed;
            this.notifyChange("speed");
        };

        update(dt: number): Vector{
            let c = this.speed,
            theta = this.direction,
            dx = c*dt*Math.cos(theta),
            dy = c*dt*Math.sin(theta);

            this.direction+= this.spin
            return new Vector(dx, dy);

        }

        public notifyChange(value = "default"): void{
            if(value=="direction"){
                console.log("Notify change direction"); 
            }
            if (this.getToolbox()!=null) {
                this.getToolbox().trigger("shapeChange", {"shape":this, "value": value});
            }
        }

    }

}