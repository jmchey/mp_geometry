namespace Geometry {

	export class Vector{
 		private name: string;
		private dx: number;
		private dy: number;

		constructor(_dx: number, _dy: number){
			this.name = "VECTOR";
			this.dx = _dx;
			this.dy = _dy;
		}

		getDx(): number{
			return this.dx;
		}

		setDx(_dx:number): void{
			this.dx = _dx;
		}

		getDy(): number{
			return this.dy;
		}

		setDy(_dy:number): void{
			this.dy = _dy;
		}

	}
}