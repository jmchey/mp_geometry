/// <reference path="./Point.ts" />
/// <reference path="./Circle.ts" />
/// <reference path="./Polygon.ts" />
/// <reference path="./Layer.ts" />

namespace Geometry {

	export class GeometryFactory{

		private static INSTANCE: GeometryFactory = null;

		private constructor(){

		}

		static getInstance(): GeometryFactory{
			if(this.INSTANCE == null)
				this.INSTANCE = new GeometryFactory();

			return this.INSTANCE;
		}

		_layerArray(options : any[]): Layer[]{
			let res: Layer[] = [];
			for(let layerOptions of options){
				res.push(this._layer(layerOptions));
			}
			return res;
		}

		_layer(options : any): Layer{
			let l = new Layer();
			l.setId(options.id);
			l.setShapes(this._shapeArray(options.shapes));
			return l;
		}

		_shapeArray(options : any[]): Shape[]{
			let res: Shape[] = [];
			for(let shapeOptions of options){
				res.push(this._shape(shapeOptions));
			}
			return res;
		}

		_shape(options: any): Shape{
			let s : Shape = null;
			switch(options.name) {
				case "POINT":
					s = new Point(options.x, options.y);
				break;

				case "POLYGON":
					let polygon = new Polygon();
					for(let p of options.points){
						polygon.addPoint(new Point(p.x, p.y))
					}
					s=polygon;
				break;

				case "CIRCLE":
					s = new Circle(new Point(options.center.x, options.center.y), options.radius);
				break;

				default:
				break;
			}
			return s;

		}



	}
}