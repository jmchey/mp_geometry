/// <reference path="../../node_modules/@types/jquery/index.d.ts" />
/// <reference path="../../node_modules/@types/lodash/index.d.ts" />
/// <reference path="./DOMObjectCreator.ts" />
/// <reference path="../Tools/Layer/LayerUI.ts" />


module MP_Geometry {
    export interface IPaletteOptions {
        baseIndex: number;
        toolbox: boolean;
        layers: Object[];
    }

    export class PaletteOptions implements IPaletteOptions {
        // Fields
        baseIndex: number = 1000;
        toolbox: boolean = false;
        layers: Object[] = null;

        constructor(args: any) {}
    }

    export class Palette {
        // Fields
        element: JQuery;
        options: PaletteOptions;
        instance: string;
        layers: Tools.LayerUI[] = [];

        constructor(element: JQuery, options: PaletteOptions) {
            this.element = element;
            this.options = options;

            this.init();
        }

        init(_options?: PaletteOptions) {
            if(_options != undefined){
                this.options = $.extend(true, this.options, _options);
            }
            //Init Layers : 
            this.options.layers.map((layerOptions:any, index:number) => {
                let l = new Tools.LayerUI(layerOptions);
                this.layers.push(l);
            });

            let domObject: any = $("<div/>", {
                "class":"container-fluid", 
                html: $("<div/>", {
                    "class":"row",
                    html: ()=>{
                        let res = [];
                        res.push(DOMObjectCreator.getCanvasContainer(this));
                        if(this.options.toolbox)
                            res.push(DOMObjectCreator.getToolBoxContainer(this));

                        return res;
                    }
                })
            });

            $(this.element).data("mp_geometry", this)
            .addClass("mp_geometry")
            .html(domObject);
        }

        addLayer(_layers: any[]): void{
            _layers.map((layerOptions:any, index:number) => {
                let l = new Tools.LayerUI(layerOptions);
                this.layers.push(l);
                this.options.layers.push(layerOptions);
            });
        }

        getLayers(_options: any): any{
            if(_options == undefined){
                return this.layers;
            }else if(typeof _options == "string"){
                return _.find(this.layers, function(layer){return _options == layer.getId()})
            }else{
                return _.filter(this.layers, function(layer) { return _.includes(_options, layer.getId());})
            }
            

        }

        getShapes(_options: any): any{

            let getShapesFromLayers = function(_layers: Tools.LayerUI[]){
                let shapes: Geometry.Shape[] = [];
                for(let layer of _layers){
                    shapes = shapes.concat(layer.getShapes());
                }
                return shapes;
            }


            return getShapesFromLayers(this.getLayers(_options));  
        }

        startRendering(_options: any): any{
            for(let l of this.getLayers(_options)){
                l.startRendering();
            }
        }

        stopRendering(_options: any): any{
            for(let l of this.getLayers(_options)){
                l.stopRendering();
            }  
        }

        startAnimation(_options: any): any{
            for(let l of this.getLayers(_options)){
                l.startAnimation();
            }
        }

        stopAnimation(_options: any): any{
            for(let l of this.getLayers(_options)){
                l.stopAnimation();
            }  
        }



        toolBox(): JQuery{
            return
        }
    }
}


//jquery plugin wrapper.
;(function(w,$){
    if(!$) return false;
    
    $.fn.extend({
        Palette: function(...args: any[]){


            if( args.length == 1 && typeof args[0]  == "object"){

                //Case options
                let _options = args[0];
                return this.each(function(){
                    let _element : JQuery = $(this);

                    if(!_element.data("mp_geometry")){
                        new MP_Geometry.Palette(_element,_options);
                    }else{
                        let INSTANCE: MP_Geometry.Palette = _element.data('mp_geometry');
                        INSTANCE.init(_options);
                    }
                });

            }else if(typeof args[0] == "string"){

                let _element : JQuery = $(this),
                 _fn = args[0],
                    _options = args[1],
                    INSTANCE = _element.data('mp_geometry');
                
                try{
                    if(INSTANCE==undefined)
                        throw new Error("Not instance of MP_Geometry.Palette");

                }catch(e){
                    console.error(e.name);
                    console.error(e.message);
                    return;
                }


                switch (_fn) {
                    case "getInstance":
                        return INSTANCE;

                    case "getLayers":
                        return INSTANCE.getLayers(_options);

                    case "getShapes":
                        return INSTANCE.getShapes(_options);

                    case "startRendering":
                        return INSTANCE.startRendering(_options);

                    case "stopRendering":
                        return INSTANCE.stopRendering(_options);

                    case "startAnimation":
                        return INSTANCE.startAnimation(_options);

                    case "stopAnimation":
                        return INSTANCE.stopAnimation(_options);

                    default:
                    // code...
                    break;
                }

                return;
            }        
        }
    })
})(window, jQuery);

