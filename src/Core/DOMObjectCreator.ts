/// <reference path="../../node_modules/@types/jquery/index.d.ts" />

/**
 *  DOMObjectCreator contains a serie of method to create JQuery components such as :
 *  "canvas" "container" or "toolbox";
 *  The mother class "Palette" uses the DOMObjectCreator to instanciate the graphical interface.  
 */
namespace DOMObjectCreator{

	/**
	 * Return the toolbox with the control panel for all the layer displayed
	 * 
	 */
	export function getToolBoxContainer(__this: any):JQuery{ 

		let toolboxContainer = $("<div/>", {
			"class":"tool-box-container col-sm-12 col-md-12 col-lg-4"
		});


		//Loop over the layers and build the dashboard
		__this.layers.map((layer:Tools.LayerUI, index:number) => {

			// Button box -- create buttons and events

			/*button select*/
			let buttonSelect = $("<button/>", {
				"class":"btn btn-secondary btn-select",
				"for":"layer_"+index,
				"text":"select",
				on: {
					"click": (e:any)=>{
						
						layer.toggleSelected();
					}
				}
			});
			/*button render*/
			let buttonRender = $("<button/>", {
				"class":"btn btn-secondary btn-render",
				"for":"layer_"+index,
				"text":"render",
				on: {
					"click": (e:any)=>{
						
						layer.toggleRendering();
					}
				}
			});
			/*button animate*/
			let buttonAnimate = $("<button/>", {
				"class":"btn btn-secondary btn-animate",
				"for":"layer_"+index,
				"text":"animate",
				on: {
					"click": (e:any)=>{
						
						layer.toggleAnimation();
					}
				}
			});
			/*button remove shapes*/
			let buttonRemoveShapes= $("<button/>", {
				"class":"btn btn-secondary btn-animate",
				"for":"layer_"+index,
				"text":"removeShapes",
				on: {
					"click": (e:any)=>{
						
						layer.removeAllShape();
					}
				}
			});
			/*button add shape*/
			let buttonShape = $("<button/>", {
				"class":"btn btn-secondary btn-animate",
				"for":"layer_"+index,
				"text":"shape",
				on: {
					"click": (e:any)=>{
						
						layer.addShape(new Geometry.Circle(new Geometry.Point(100,100), 50));
					}
				}
			});
			/*button draw shape*/
			let buttonDraw = $("<button/>", {
				"class":"btn btn-secondary btn-animate",
				"for":"layer_"+index,
				"text":"Draw",
				on: {
					"click": (e:any)=>{
						
						layer.draw();
					}
				}
			});
			/*button add shape*/
			let buttonErase = $("<button/>", {
				"class":"btn btn-secondary btn-animate",
				"for":"layer_"+index,
				"text":"erase",
				on: {
					"click": (e:any)=>{
						
						layer.erase();
					}
				}
			});


			let buttonBox = $("<div/>",{
				"class":"btn-group layer-toolbox",
				"role":"group",
				"aria-label":"Tool box",
				html: [buttonAnimate, buttonRender, buttonRemoveShapes, buttonSelect, buttonShape, buttonDraw, buttonErase]
			});

			// Header 

			let header = $("<h2/>", { "class":"h2", text : layer.getId()});

			// Shape box -- shapeList
			let shapeBox = $("<div/>",{
				"class":"",
				"role":"group",
				"aria-label":"Tool box",
				html: [
					$("<h3>",{
						"class":"", 
						text: "# list of shapes"
					}),
					$("<div/>",{
						"id":"shape-accordion",
						"class":"shapeList accordion",
						html : function(){
						var res = [];
						for (let shape of layer.getShapes()){
							res.push(getShapeDetail(shape));
						}
						return res;
					}
				})],
				on: {
					"notifyShapeAdded": function(e:any){
					}
				}

			});
			layer.setToolbox(shapeBox);

			//Append dashboard into tools container
			toolboxContainer.append([header, buttonBox, shapeBox]);
		});

		return  toolboxContainer;
  

	};


	/**
	 * Return a JQuery component refered as 'toolbox' or 'shapeDetail" and that displays 
	 * The properties of the shape given as parameter. 
	 * @param shape, the Shape you want to display the detail.
	 */
	function getShapeDetail(shape: Geometry.Shape):JQuery{
		/**
		 * Unwrap in case of decoration get the row shape to display the data.
		 */
		if(shape instanceof Tools.ShapeCtrl){
			shape = shape.getShape();
		}

		//List of shape properties that we want to display in the details
		let innerProperties = [	
			getPropertyFormGroup("Id", shape.getId(), true),
			getPropertyFormGroup("Name", shape.getName(), true),
			getPropertyFormGroup("Speed", shape.getSpeed()),
			getPropertyFormGroup("Direction", shape.getDirection()),
			getPropertyFormGroup("Spin", shape.getSpin()),
			getPropertyFormGroup("Color", shape.getColor())];
		
		if(shape instanceof Geometry.Circle){
			innerProperties.push(getPropertyFormGroup("Radius", shape.getRadius()));
		}


		var shapeDetail =  $("<div/>",{
			"class":"card",
			html: [
				$("<div/>",{
					"class":"card-header", 
					"id":"",
					html: $("<h2/>", {
						"class":"mb-0", 
						html : 	$("<button/>", {
							"class":"btn btn-link", 
							"type":"button", 
							"data-toggle":"collapse", 
							"data-target": "#collapse-"+shape.getId(), 
							"aria-expanded" : "true",
							"aria-controls": "collapse-"+shape.getId(),
							text : "Shape #"+shape.getId()
						})
					})
				}),
				$("<div/>",{
					"class":"collapse", 
					"id":"collapse-"+shape.getId(),
					"aria-labelledby":"headingOne",
					html : $("<div/>",{
						"class":"card-body row shape-detail-container",
						html: [
							$("<div/>", { 
								"class":"col-sm-12 col-md-6",
								html: $("<form/>", {
									html : innerProperties,
									on : {
										"change" : function(e:any){
											console.log(e);
											console.info("Changing value");
											var name = e.target.name;
											var value = e.target.value;
											switch(name){
												case "Direction": {
													shape.setDirection(value);
													break;
												}
												case "Speed": {
													shape.setSpeed(value);
													break;
												}
												case "Spin": {
													shape.setSpin(value);
													break;
												}
												case "Color": {
													shape.setColor(value);
													break;
												}
												case "Radius": {
													if(shape instanceof Geometry.Circle){
														shape.setRadius(value);
													}
													break;
												}
												default : {
			
												}
			
			
											}
										}
									}
								}),
							}),
							$("<div/>", { 
								"class":"col-sm-12 col-md-6",
								html: $("<form/>", {
									html : getSpecificProperties(shape),
									on : {
										"change" : function(e:JQueryEventObject){
											switch(name){
												case "radius": {
													break;
												}
												default : {
			
												}
			
			
											}
										}
									}
								}),
							}),
			
						],
						on: {
							"shapeChange": function(e:any, data:any){
								console.log("Notified");
								var shape = e.data;
								console.log(e, data);
								$(this).find("[name='Name']").val(data.shape.getName());
								$(this).find("[name='Speed']").val(data.shape.getSpeed());
								$(this).find("[name='Direction']").val(data.shape.getDirection());
								$(this).find("[name='Spin']").val(data.shape.getSpin());
								$(this).find("[name='Color']").val(data.shape.getColor());
								if(shape instanceof Geometry.Circle){
									$(this).find("[name='Radius']").val(data.shape.getRadius());
								}
							}
						}
					
					})

				})
			]
		});
		shape.setToolbox(shapeDetail);

		return shapeDetail;

	}

	/**
	 * Return a JQuery component that displays the specific properties of the Shape
	 * @param shape the shape you want to be displayed in the toolbox
	 */
	function getSpecificProperties(shape: Geometry.Shape):JQuery[]{
		let res = [];

		if(shape instanceof Geometry.Circle)
		{
			let centerInput = getPointInput(shape.getCenter(), "center");
			shape.getCenter().setToolbox(centerInput);
			res.push(centerInput);


		}else if(shape instanceof Geometry.Polygon){
			console.log("Ma god dayum Poly");
			for(let point of shape.getPoints()){
				let pointInput = getPointInput(point, "point");
				point.setToolbox(pointInput);
				res.push(pointInput);	
			}
		}else{
			res = [];
		}
		return res;
	}

	function getPointInput(point: Geometry.Point, name : string): JQuery{
		return $("<div/>", {

			"class": "row",
			html: [
				$("<label/>", {	text :name, "class":"col-sm-12" }),
				$("<label/>", {
					"class":"col-sm-6 col-form-label col-form-label-sm",
					"for": "x",
					text : "X"
				}),
				$("<div/>", {
					"class" : "col-sm-6",
					html: $("<input/>", {
						"class":"form-control form-control-sm",
						"id":"x",
						"placeholder": "X value",
						"name":"x",
						value : point.getX(),
						on : {
							"change" : function(e: any){
								let value = e.target.value;
								point.setX(value);
								e.stopPropagation();
							}
						}
					})
				}),
				$("<label/>", {
					"class":"col-sm-6 col-form-label col-form-label-sm",
					"for": "y",
					text : "Y"
				}),
				$("<div/>", {
					"class" : "col-sm-6",
					html: $("<input/>", {
						"class":"form-control form-control-sm",
						"id":"y",
						"placeholder": "Y value",
						"name":"y",
						value : point.getY(),
						on : {
							"change" : function(e: any){
								let value = e.target.value;
								point.setY(value);
								e.stopPropagation();
							}
						}
					})
				})
			],
			on : {
				"shapeChange": function(e:any, data:any){
					var shape = e.data;
					$(this).find("[name='x']").val(data.shape.getX());
					$(this).find("[name='y']").val(data.shape.getY());
					e.stopPropagation();
				}
			}
		});
	}


	/**
	 * Retrun a Jquery component that displays the properties of the mother class 'Shape'
	 * @param name the name of the property
	 * @param value the value of the property
	 */
	function getPropertyFormGroup(name : string, value: any, disabled = false ): JQuery{
		return $("<div/>", {
			"class": "row",
			html: [
				$("<label/>", {
					"class":"col-sm-6 col-form-label col-form-label-sm",
					"for": name,
					text : name
				}),
				$("<div/>", {
					"class" : "col-sm-6",
					html: $("<input/>", {
						"class":"form-control form-control-sm",
						"id":name,
						"placeholder": name+" value",
						"name":name,
						value : value	
					}).prop('disabled', disabled)
				})
			]
		})
	}


	/**
	 * 
	 * 
	 */
	export function getCanvasContainer(__this: any):JQuery{
		return $("<div/>", {
			"class":(__this.options.toolbox)?"col-sm-12 col-md-12 col-lg-8":"col-sm-12",
			"height":__this.options.height,
			"width":__this.options.width,
			html: ()=> {
				let layers:JQuery[]=[];
				__this.layers.map((layer:Tools.LayerUI, index:number) => {
					layers.push(layer.getCanvas());
				});
				return layers;
			}
		});
	}

	/**
	 * 
	 * 
	 */
	export function getCanvas(id?: string, height?:number, width?:number, zindex?:number ):JQuery{

		let canvas =  $("<canvas/>",{
			"id":id,
			"height": 
			"class":"layer" 
		}).css({
			"zIndex":zindex,
			"position":"absolute",
			"top":"0px"
		});

		return canvas;
	}
}