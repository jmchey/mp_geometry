/// <reference path="./Geometry/Layer.ts" />
/// <reference path="./Geometry/Shape.ts" />
/// <reference path="./Geometry/Circle.ts" />
/// <reference path="./Geometry/Polygon.ts" />
/// <reference path="./Geometry/Point.ts" />
/// <reference path="./Geometry/Vector.ts" />
/// <reference path="./Geometry/GeometryFactory.ts" />
module.exports = Geometry;