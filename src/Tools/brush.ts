/// <reference path="../Geometry/Shape.ts" />
/// <reference path="../Geometry/Circle.ts" />
/// <reference path="../Geometry/Polygon.ts" />
/// <reference path="./ShapeCtrl.ts" />

namespace Tools {
	export class Brush {

		private ctx: any;
		private lineWidth: number = 1;
		private fillStyle: string = "#FA0B16";
		private strokeStyle: string = "#FA0B16";
		private fill: boolean = true; 

		constructor(_ctx?:Object){
			if(_ctx!=null)
				this.setCtx(_ctx);		
		}

		getRatio(): number{
			return this.ctx.canvas.width / 1600;
		}

		drawShape(_shape: Geometry.ShapeInterface): void{
			this.setStrokeStyle(_shape.getColor());
			this.setFillStyle(_shape.getColor());
			if(_shape.isSelected()){
				this.setStrokeStyle("red");
			}
			if(_shape instanceof Tools.ShapeCtrl)
				_shape = _shape.getShape();
			if (_shape instanceof Geometry.Polygon){			
				let pointArray = _shape.getPoints();
				let p0 =  pointArray[pointArray.length-1];
				this.ctx.beginPath();
				this.ctx.moveTo(p0.getX()*this.getRatio(), p0.getY()*this.getRatio());
				for( let i in _shape.getPoints()) {
					let p = _shape.getPoints()[i];
					this.ctx.lineTo(p.getX()*this.getRatio(), p.getY()*this.getRatio());
				}
				this.ctx.stroke();
			}else if(_shape instanceof Geometry.Circle){
				let circle = <Geometry.Circle> _shape;
				this.ctx.beginPath();
				this.ctx.arc(circle.getCenter().getX()*this.getRatio(), circle.getCenter().getY()*this.getRatio(), circle.getRadius()*this.getRatio(), 0, 2 * Math.PI, false);
				this.ctx.fill();
				this.ctx.stroke();
			}else{
			}
		}

		//Getter & setter
		getCtx(): Object{
			return this.ctx;
		}

		setCtx(_ctx:Object): void{
			this.ctx = _ctx;
			this.ctx.fillStyle = this.fillStyle;
			this.ctx.lineWidth = this.lineWidth;
			this.ctx.strokeStyle = this.strokeStyle;
		}

		getLineWidth(): number{
			return this.lineWidth;
		}

		seLineWidth(_lineWidth:number): void{
			this.lineWidth = _lineWidth;
			this.ctx.lineWidth = this.lineWidth;
		}

		getFillStyle(): string{
			return this.fillStyle;
		}

		setFillStyle(_fillStyle:string): void{
			this.fillStyle = _fillStyle;
			this.ctx.fillStyle = this.fillStyle;
		}

		getStrokeStyle(): string{
			return this.strokeStyle;
		}

		setStrokeStyle(_strokeStyle:string): void{
			this.strokeStyle = _strokeStyle;
			this.ctx.strokeStyle = this.strokeStyle;
		}
	}
}