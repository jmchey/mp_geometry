/// <reference path="../../node_modules/@types/jquery/index.d.ts" />
/// <reference path="../Geometry/Shape.ts" />
/// <reference path="./ShapeCtrl.ts" />



namespace Tools {
	export class ShapeLinearCtrlUI extends ShapeCtrl{

		constructor(_shape: Geometry.Shape){
			super(_shape);
		}

        update(dt: number): Geometry.Vector{
        	if(this.leftPressed)
        		this.setSpeed(this.getSpeed()-this._delta);
        	if(this.rightPressed)
        		this.setSpeed(this.getSpeed()+this._delta);
			return this.shape.update(dt);

        }


	}
}