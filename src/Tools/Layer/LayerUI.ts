/// <reference path="../../Geometry/Layer.ts" />
/// <reference path="../Brush.ts" />
/// <reference path="../../Core/DOMObjectCreator.ts" />



namespace Tools {
	export class LayerUI {

		/** Decorated Layer **/
		private layer: Geometry.Layer;
		
		/** UI properties **/
		private renderRequestID: any;
		private rendering: boolean;
		protected canvas: any;
		protected ctx: any;
		protected brush : Brush;
		protected ratio: number = 16/10;
		protected _ratio: number = 10/16;
		protected width: number;
		protected height: number;

		private updateIntervalId: any;
		private animated: boolean;
		private dt: number = 20;

		private toolbox: JQuery;
		protected selected : boolean = true;


		constructor(arg0: any, arg1?:any){
			let options: any;
			if(arg0 instanceof Geometry.Layer){
				this.layer = arg0;
				options = (arg1 == undefined)?{}:arg1;
			}else{
				this.layer = new Geometry.Layer(arg0);
				options = arg0;
			}
			this.setShapes((options != undefined && options.shapes)?options.shapes: []);
			/** UI initialisation **/
			this.canvas = (options.canvas)?options.canvas:DOMObjectCreator.getCanvas();
			//this.toolbox = (options.toolbox)?options.toolbox:DOMObjectCreator.getTool(this.width, this.height);
			this.ctx = this.canvas[0].getContext("2d");
			this.brush = new Brush(this.ctx);
			$(this.canvas).ready((e)=>{
				this.resize();
			})
			$(window).on('resize', (e)=>{
				this.resize();
			})

		}

		/** Déeorated methods **/
		getId(): string{
			return this.layer.getId();
		}

		setId(_id: string){
			this.layer.setId(_id);
		}

		getShapes(): Geometry.Shape[]{
			return this.layer.getShapes();
		}

		setShapes(_shapes: Geometry.Shape[]): void{
			for(let shape of _shapes){
				this.addShape(shape);
			}	
		}

		addShape(_shape: Geometry.Shape): void{
			this.layer.addShape(_shape);
			_shape.setParentLayer(this);
		}

		removeShape(_shape: Geometry.Shape): void{
			this.layer.removeShape(_shape);
		}

		removeAllShape(): void{
			this.layer.removeAllShape();
			this.notifyShapeChange();
		}

		
		/** UI Specifique methods**/
		setHeight(_height: number): void{
			this.height = _height
			this.canvas.attr("height",_height.toString()); 
		}

		getHeight(): number{
			return this.height;
		}

		setWidth(_width: number): void{
			this.width = _width
			this.canvas.attr("width",_width.toString()); 
		}

		getWidth(): number{
			return this.width; 
		}

		getCanvas(): JQuery{
			return this.canvas;
		}

		setCanvas(_canvas:JQuery): void{
			this.canvas = _canvas;
			this.ctx = this.canvas[0].getContext("2d");
			this.brush = new Brush(this.ctx);
		}

		getToolbox(): JQuery{
			return this.toolbox;
		}

		setToolbox(_toolbox:JQuery): void{
			this.toolbox = _toolbox;
		}

		getBrush(): Brush{
			return this.brush;
		}

		seBrush(_brush: Brush): void{
			this.brush = _brush;
		}

		toggleSelected(): void{
			this.selected = !this.selected;
			this.canvas.toggleClass("selected");
		}

		//Methods
		draw(): void{
			for(let shape of this.getShapes()){
				this.brush.drawShape(shape);
			}
		}

		erase(): void{
			this.ctx.clearRect(0, 0, this.width, this.height);
		}

		render(): void{
			this.erase();
			this.draw();
		}

		update(){
			for(let shape of this.getShapes() ){
				shape.update(this.dt);
			}
		}

		startAnimation(): void{
			let __this = this;
			this.updateIntervalId = setInterval(function(){
				for(let shape of __this.getShapes() ){
					shape.update(__this.dt);
				}
			}, __this.dt)
		}

		stopAnimation(): void{
			clearInterval(this.updateIntervalId);
		}

		toggleAnimation(): void{
			(this.animated)?this.stopAnimation():this.startAnimation();
			this.animated = !this.animated;
		}

		startRendering():void{
			let __this = this;
			function step(timestamp: number) {
				__this.render();
				__this.renderRequestID = requestAnimationFrame(step);
			}
			__this.renderRequestID = requestAnimationFrame(step);
		}

		stopRendering():void{
			cancelAnimationFrame(this.renderRequestID);
		}

		toggleRendering(): void{
			(this.rendering)?this.stopRendering():this.startRendering();
			this.rendering = !this.rendering;
		}

		resize(): void{
			this.setWidth(this.canvas.parent().width());
			this.setHeight(this.canvas.parent().width()*this._ratio);
		}

		/* Listener */
		notifyShapeChange(): void{
			if(this.getToolbox()!=undefined){
				console.log("trigger 'notifyShapeChange'");
				this.getToolbox().trigger("notifyShapeChange");
			}
		};
	}
}