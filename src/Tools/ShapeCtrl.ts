/// <reference path="../Geometry/ShapeInterface.ts" />

namespace Tools {
	export class ShapeCtrl implements Geometry.ShapeInterface{

		/** Decorated Shape **/
		protected shape: Geometry.Shape;

		/** UI controls **/
		protected leftCtrl: number = 37;
		protected rightCtrl: number = 39;
		protected leftPressed: boolean = false;
		protected rightPressed: boolean = false;
		protected _delta : number = 0.05;

		constructor(_shape: Geometry.Shape){
			this.shape = _shape;

			$(document).on("keydown", (e)=>{
				switch (e.which) {
					case this.rightCtrl:
						console.log("keyPressed right");
						this.rightPressed = true;
						break;

					case this.leftCtrl:
						console.log("keyPressed left");
						this.leftPressed = true;
						break;
					
					default:
						console.log("keyPressed");
						break;
				}

			}).on('keyup', (e)=>{
				switch (e.which) {
					case this.rightCtrl:
						console.log("keyUP right");
						this.rightPressed = false;
						break;

					case this.leftCtrl:
						console.log("keyUP left");
						this.leftPressed = false;
						break;
					
					default:
						console.log("keyup");
						break;
				}
			})
		}

		getShape(): Geometry.Shape{
			return this.shape;
		}

		/** Decorated getter methods **/
		getType(): string{
			return this.shape.getType();
		}

		getId(): any{
            return this.shape.getId();
		};

		setToolbox(toolbox : JQuery) : void{
            this.shape.setToolbox(toolbox);
        }

        getToolbox() : JQuery{
            return this.getToolbox();
        }
		
		setSelected(selected: boolean) : void{
			this.shape.setSelected(selected);
		}
		
		isSelected(): boolean{
			return this.shape.isSelected();
		}

		setParentLayer(parentLayer: Tools.LayerUI): void{
            return this.shape.setParentLayer(parentLayer);
        }

        getParentLayer(): Tools.LayerUI{
            return this.shape.getParentLayer();
        }

        getName(): string{
            return this.shape.getName();
        };

        setName(_name: string): void{
            this.shape.setName(_name);
        };

        getColor(): string{
            return this.shape.getColor();
        };

        setColor(_color: string): void{
            this.shape.setColor(_color);
        };

        getDirection(): number{
			return this.getShape().getDirection();
		}

		setDirection(_direction : number): void{
			this.getShape().setDirection(_direction);
		}

		getSpeed(): number{
			return this.getShape().getSpeed();
		}

		setSpeed(_speed : number): void{
			this.getShape().setSpeed(_speed);
		}

		getSpin(): number{
			return this.shape.getSpin();
		}

		setSpin(spin : number) : void {
			this.shape.setSpin(spin);
		}
		getCenter(): Geometry.Point {
			if (this.shape instanceof Geometry.Circle){
				return this.shape.getCenter();
			}
			return null;
		}

		/** Specific methods **/
		getLeftCtrl(): any{
			return this.leftCtrl;
		}

		setLeftCtrl(_leftCtrl: any){
			this.leftCtrl = _leftCtrl;
		}

		getRightCtrl(): any{
			return this.rightCtrl;
		}

		setRightCtrl(_rightCtrl: any){
			this.rightCtrl = _rightCtrl;
		}
	}
}