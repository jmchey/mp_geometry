/// <reference path="../../node_modules/@types/jquery/index.d.ts" />
/// <reference path="../Geometry/Shape.ts" />
/// <reference path="./ShapeCtrl.ts" />



namespace Tools {
	export class ShapeCircularCtrlUI extends ShapeCtrl{

	
		constructor(_shape: Geometry.Shape){
			super(_shape);
		}


        update(dt: number): Geometry.Vector{
        	if(this.leftPressed)
        		this.setDirection(this.getDirection()-this._delta);
        	if(this.rightPressed)
        		this.setDirection(this.getDirection()+this._delta);
			return this.shape.update(dt);

        }

	}
}