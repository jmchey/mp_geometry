var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Geometry;
(function (Geometry) {
    var Vector = (function () {
        function Vector(_dx, _dy) {
            this.name = "VECTOR";
            this.dx = _dx;
            this.dy = _dy;
        }
        Vector.prototype.getDx = function () {
            return this.dx;
        };
        Vector.prototype.setDx = function (_dx) {
            this.dx = _dx;
        };
        Vector.prototype.getDy = function () {
            return this.dy;
        };
        Vector.prototype.setDy = function (_dy) {
            this.dy = _dy;
        };
        return Vector;
    }());
    Geometry.Vector = Vector;
})(Geometry || (Geometry = {}));
var Geometry;
(function (Geometry) {
    var Shape = (function () {
        function Shape(_name, _color) {
            if (_color === void 0) { _color = "#aaa"; }
            this.selected = false;
            this.spin = 0.00;
            this.direction = 0;
            this.speed = 0.1;
            this.id = Shape.generateId();
            this.name = _name;
            this.color = _color;
        }
        ;
        Shape.prototype.getId = function () {
            return this.id;
        };
        ;
        Shape.prototype.setToolbox = function (toolbox) {
            this.toolbox = toolbox;
        };
        Shape.prototype.getToolbox = function () {
            return this.toolbox;
        };
        Shape.prototype.setSelected = function (selected) {
            this.selected = selected;
            this.notifyChange("selected");
        };
        Shape.prototype.isSelected = function () {
            return this.selected;
        };
        Shape.prototype.getParentLayer = function () {
            return this.parentLayer;
        };
        Shape.prototype.setParentLayer = function (parentLayer) {
            this.parentLayer = parentLayer;
        };
        Shape.prototype.getName = function () {
            return this.name;
        };
        ;
        Shape.prototype.setName = function (_name) {
            this.name = _name;
            this.notifyChange("name");
        };
        ;
        Shape.prototype.getColor = function () {
            return this.color;
        };
        ;
        Shape.prototype.setColor = function (_color) {
            this.color = _color;
            this.notifyChange("color");
        };
        ;
        Shape.prototype.getSpin = function () {
            return this.spin;
        };
        ;
        Shape.prototype.setSpin = function (_spin) {
            this.spin = _spin;
            this.notifyChange("spin");
        };
        ;
        Shape.prototype.getDirection = function () {
            return this.direction;
        };
        ;
        Shape.prototype.setDirection = function (_direction) {
            this.direction = _direction;
            this.notifyChange("direction");
        };
        ;
        Shape.prototype.getSpeed = function () {
            return this.speed;
        };
        ;
        Shape.prototype.setSpeed = function (_speed) {
            this.speed = _speed;
            this.notifyChange("speed");
        };
        ;
        Shape.prototype.update = function (dt) {
            var c = this.speed, theta = this.direction, dx = c * dt * Math.cos(theta), dy = c * dt * Math.sin(theta);
            this.direction += this.spin;
            return new Geometry.Vector(dx, dy);
        };
        Shape.prototype.notifyChange = function (value) {
            if (value === void 0) { value = "default"; }
            if (value == "direction") {
                console.log("Notify change direction");
            }
            if (this.getToolbox() != null) {
                this.getToolbox().trigger("shapeChange", { "shape": this, "value": value });
            }
        };
        Shape.generateId = (function () {
            var private_count = 0;
            var increment = function () {
                private_count += 1;
                return private_count;
            };
            return increment;
        })();
        return Shape;
    }());
    Geometry.Shape = Shape;
})(Geometry || (Geometry = {}));
var Tools;
(function (Tools) {
    var ShapeCtrl = (function () {
        function ShapeCtrl(_shape) {
            var _this = this;
            this.leftCtrl = 37;
            this.rightCtrl = 39;
            this.leftPressed = false;
            this.rightPressed = false;
            this._delta = 0.05;
            this.shape = _shape;
            $(document).on("keydown", function (e) {
                switch (e.which) {
                    case _this.rightCtrl:
                        console.log("keyPressed right");
                        _this.rightPressed = true;
                        break;
                    case _this.leftCtrl:
                        console.log("keyPressed left");
                        _this.leftPressed = true;
                        break;
                    default:
                        console.log("keyPressed");
                        break;
                }
            }).on('keyup', function (e) {
                switch (e.which) {
                    case _this.rightCtrl:
                        console.log("keyUP right");
                        _this.rightPressed = false;
                        break;
                    case _this.leftCtrl:
                        console.log("keyUP left");
                        _this.leftPressed = false;
                        break;
                    default:
                        console.log("keyup");
                        break;
                }
            });
        }
        ShapeCtrl.prototype.getShape = function () {
            return this.shape;
        };
        ShapeCtrl.prototype.getType = function () {
            return this.shape.getType();
        };
        ShapeCtrl.prototype.getId = function () {
            return this.shape.getId();
        };
        ;
        ShapeCtrl.prototype.setToolbox = function (toolbox) {
            this.shape.setToolbox(toolbox);
        };
        ShapeCtrl.prototype.getToolbox = function () {
            return this.getToolbox();
        };
        ShapeCtrl.prototype.setSelected = function (selected) {
            this.shape.setSelected(selected);
        };
        ShapeCtrl.prototype.isSelected = function () {
            return this.shape.isSelected();
        };
        ShapeCtrl.prototype.setParentLayer = function (parentLayer) {
            return this.shape.setParentLayer(parentLayer);
        };
        ShapeCtrl.prototype.getParentLayer = function () {
            return this.shape.getParentLayer();
        };
        ShapeCtrl.prototype.getName = function () {
            return this.shape.getName();
        };
        ;
        ShapeCtrl.prototype.setName = function (_name) {
            this.shape.setName(_name);
        };
        ;
        ShapeCtrl.prototype.getColor = function () {
            return this.shape.getColor();
        };
        ;
        ShapeCtrl.prototype.setColor = function (_color) {
            this.shape.setColor(_color);
        };
        ;
        ShapeCtrl.prototype.getDirection = function () {
            return this.getShape().getDirection();
        };
        ShapeCtrl.prototype.setDirection = function (_direction) {
            this.getShape().setDirection(_direction);
        };
        ShapeCtrl.prototype.getSpeed = function () {
            return this.getShape().getSpeed();
        };
        ShapeCtrl.prototype.setSpeed = function (_speed) {
            this.getShape().setSpeed(_speed);
        };
        ShapeCtrl.prototype.getSpin = function () {
            return this.shape.getSpin();
        };
        ShapeCtrl.prototype.setSpin = function (spin) {
            this.shape.setSpin(spin);
        };
        ShapeCtrl.prototype.getCenter = function () {
            if (this.shape instanceof Geometry.Circle) {
                return this.shape.getCenter();
            }
            return null;
        };
        ShapeCtrl.prototype.getLeftCtrl = function () {
            return this.leftCtrl;
        };
        ShapeCtrl.prototype.setLeftCtrl = function (_leftCtrl) {
            this.leftCtrl = _leftCtrl;
        };
        ShapeCtrl.prototype.getRightCtrl = function () {
            return this.rightCtrl;
        };
        ShapeCtrl.prototype.setRightCtrl = function (_rightCtrl) {
            this.rightCtrl = _rightCtrl;
        };
        return ShapeCtrl;
    }());
    Tools.ShapeCtrl = ShapeCtrl;
})(Tools || (Tools = {}));
var Tools;
(function (Tools) {
    var ShapeCircularCtrlUI = (function (_super) {
        __extends(ShapeCircularCtrlUI, _super);
        function ShapeCircularCtrlUI(_shape) {
            return _super.call(this, _shape) || this;
        }
        ShapeCircularCtrlUI.prototype.update = function (dt) {
            if (this.leftPressed)
                this.setDirection(this.getDirection() - this._delta);
            if (this.rightPressed)
                this.setDirection(this.getDirection() + this._delta);
            return this.shape.update(dt);
        };
        return ShapeCircularCtrlUI;
    }(Tools.ShapeCtrl));
    Tools.ShapeCircularCtrlUI = ShapeCircularCtrlUI;
})(Tools || (Tools = {}));
var Tools;
(function (Tools) {
    var ShapeLinearCtrlUI = (function (_super) {
        __extends(ShapeLinearCtrlUI, _super);
        function ShapeLinearCtrlUI(_shape) {
            return _super.call(this, _shape) || this;
        }
        ShapeLinearCtrlUI.prototype.update = function (dt) {
            if (this.leftPressed)
                this.setSpeed(this.getSpeed() - this._delta);
            if (this.rightPressed)
                this.setSpeed(this.getSpeed() + this._delta);
            return this.shape.update(dt);
        };
        return ShapeLinearCtrlUI;
    }(Tools.ShapeCtrl));
    Tools.ShapeLinearCtrlUI = ShapeLinearCtrlUI;
})(Tools || (Tools = {}));
var DOMObjectCreator;
(function (DOMObjectCreator) {
    function getToolBoxContainer(__this) {
        var toolboxContainer = $("<div/>", {
            "class": "tool-box-container col-sm-12 col-md-12 col-lg-4"
        });
        __this.layers.map(function (layer, index) {
            var buttonSelect = $("<button/>", {
                "class": "btn btn-secondary btn-select",
                "for": "layer_" + index,
                "text": "select",
                on: {
                    "click": function (e) {
                        layer.toggleSelected();
                    }
                }
            });
            var buttonRender = $("<button/>", {
                "class": "btn btn-secondary btn-render",
                "for": "layer_" + index,
                "text": "render",
                on: {
                    "click": function (e) {
                        layer.toggleRendering();
                    }
                }
            });
            var buttonAnimate = $("<button/>", {
                "class": "btn btn-secondary btn-animate",
                "for": "layer_" + index,
                "text": "animate",
                on: {
                    "click": function (e) {
                        layer.toggleAnimation();
                    }
                }
            });
            var buttonRemoveShapes = $("<button/>", {
                "class": "btn btn-secondary btn-animate",
                "for": "layer_" + index,
                "text": "removeShapes",
                on: {
                    "click": function (e) {
                        layer.removeAllShape();
                    }
                }
            });
            var buttonShape = $("<button/>", {
                "class": "btn btn-secondary btn-animate",
                "for": "layer_" + index,
                "text": "shape",
                on: {
                    "click": function (e) {
                        layer.addShape(new Geometry.Circle(new Geometry.Point(100, 100), 50));
                    }
                }
            });
            var buttonDraw = $("<button/>", {
                "class": "btn btn-secondary btn-animate",
                "for": "layer_" + index,
                "text": "Draw",
                on: {
                    "click": function (e) {
                        layer.draw();
                    }
                }
            });
            var buttonErase = $("<button/>", {
                "class": "btn btn-secondary btn-animate",
                "for": "layer_" + index,
                "text": "erase",
                on: {
                    "click": function (e) {
                        layer.erase();
                    }
                }
            });
            var buttonBox = $("<div/>", {
                "class": "btn-group layer-toolbox",
                "role": "group",
                "aria-label": "Tool box",
                html: [buttonAnimate, buttonRender, buttonRemoveShapes, buttonSelect, buttonShape, buttonDraw, buttonErase]
            });
            var header = $("<h2/>", { "class": "h2", text: layer.getId() });
            var shapeBox = $("<div/>", {
                "class": "",
                "role": "group",
                "aria-label": "Tool box",
                html: [
                    $("<h3>", {
                        "class": "",
                        text: "# list of shapes"
                    }),
                    $("<div/>", {
                        "id": "shape-accordion",
                        "class": "shapeList accordion",
                        html: function () {
                            var res = [];
                            for (var _i = 0, _a = layer.getShapes(); _i < _a.length; _i++) {
                                var shape = _a[_i];
                                res.push(getShapeDetail(shape));
                            }
                            return res;
                        }
                    })
                ],
                on: {
                    "notifyShapeAdded": function (e) {
                    }
                }
            });
            layer.setToolbox(shapeBox);
            toolboxContainer.append([header, buttonBox, shapeBox]);
        });
        return toolboxContainer;
    }
    DOMObjectCreator.getToolBoxContainer = getToolBoxContainer;
    ;
    function getShapeDetail(shape) {
        if (shape instanceof Tools.ShapeCtrl) {
            shape = shape.getShape();
        }
        var innerProperties = [
            getPropertyFormGroup("Id", shape.getId(), true),
            getPropertyFormGroup("Name", shape.getName(), true),
            getPropertyFormGroup("Speed", shape.getSpeed()),
            getPropertyFormGroup("Direction", shape.getDirection()),
            getPropertyFormGroup("Spin", shape.getSpin()),
            getPropertyFormGroup("Color", shape.getColor())
        ];
        if (shape instanceof Geometry.Circle) {
            innerProperties.push(getPropertyFormGroup("Radius", shape.getRadius()));
        }
        var shapeDetail = $("<div/>", {
            "class": "card",
            html: [
                $("<div/>", {
                    "class": "card-header",
                    "id": "",
                    html: $("<h2/>", {
                        "class": "mb-0",
                        html: $("<button/>", {
                            "class": "btn btn-link",
                            "type": "button",
                            "data-toggle": "collapse",
                            "data-target": "#collapse-" + shape.getId(),
                            "aria-expanded": "true",
                            "aria-controls": "collapse-" + shape.getId(),
                            text: "Shape #" + shape.getId()
                        })
                    })
                }),
                $("<div/>", {
                    "class": "collapse",
                    "id": "collapse-" + shape.getId(),
                    "aria-labelledby": "headingOne",
                    html: $("<div/>", {
                        "class": "card-body row shape-detail-container",
                        html: [
                            $("<div/>", {
                                "class": "col-sm-12 col-md-6",
                                html: $("<form/>", {
                                    html: innerProperties,
                                    on: {
                                        "change": function (e) {
                                            console.log(e);
                                            console.info("Changing value");
                                            var name = e.target.name;
                                            var value = e.target.value;
                                            switch (name) {
                                                case "Direction": {
                                                    shape.setDirection(value);
                                                    break;
                                                }
                                                case "Speed": {
                                                    shape.setSpeed(value);
                                                    break;
                                                }
                                                case "Spin": {
                                                    shape.setSpin(value);
                                                    break;
                                                }
                                                case "Color": {
                                                    shape.setColor(value);
                                                    break;
                                                }
                                                case "Radius": {
                                                    if (shape instanceof Geometry.Circle) {
                                                        shape.setRadius(value);
                                                    }
                                                    break;
                                                }
                                                default: {
                                                }
                                            }
                                        }
                                    }
                                })
                            }),
                            $("<div/>", {
                                "class": "col-sm-12 col-md-6",
                                html: $("<form/>", {
                                    html: getSpecificProperties(shape),
                                    on: {
                                        "change": function (e) {
                                            switch (name) {
                                                case "radius": {
                                                    break;
                                                }
                                                default: {
                                                }
                                            }
                                        }
                                    }
                                })
                            }),
                        ],
                        on: {
                            "shapeChange": function (e, data) {
                                console.log("Notified");
                                var shape = e.data;
                                console.log(e, data);
                                $(this).find("[name='Name']").val(data.shape.getName());
                                $(this).find("[name='Speed']").val(data.shape.getSpeed());
                                $(this).find("[name='Direction']").val(data.shape.getDirection());
                                $(this).find("[name='Spin']").val(data.shape.getSpin());
                                $(this).find("[name='Color']").val(data.shape.getColor());
                                if (shape instanceof Geometry.Circle) {
                                    $(this).find("[name='Radius']").val(data.shape.getRadius());
                                }
                            }
                        }
                    })
                })
            ]
        });
        shape.setToolbox(shapeDetail);
        return shapeDetail;
    }
    function getSpecificProperties(shape) {
        var res = [];
        if (shape instanceof Geometry.Circle) {
            var centerInput = getPointInput(shape.getCenter(), "center");
            shape.getCenter().setToolbox(centerInput);
            res.push(centerInput);
        }
        else if (shape instanceof Geometry.Polygon) {
            console.log("Ma god dayum Poly");
            for (var _i = 0, _a = shape.getPoints(); _i < _a.length; _i++) {
                var point = _a[_i];
                var pointInput = getPointInput(point, "point");
                point.setToolbox(pointInput);
                res.push(pointInput);
            }
        }
        else {
            res = [];
        }
        return res;
    }
    function getPointInput(point, name) {
        return $("<div/>", {
            "class": "row",
            html: [
                $("<label/>", { text: name, "class": "col-sm-12" }),
                $("<label/>", {
                    "class": "col-sm-6 col-form-label col-form-label-sm",
                    "for": "x",
                    text: "X"
                }),
                $("<div/>", {
                    "class": "col-sm-6",
                    html: $("<input/>", {
                        "class": "form-control form-control-sm",
                        "id": "x",
                        "placeholder": "X value",
                        "name": "x",
                        value: point.getX(),
                        on: {
                            "change": function (e) {
                                var value = e.target.value;
                                point.setX(value);
                                e.stopPropagation();
                            }
                        }
                    })
                }),
                $("<label/>", {
                    "class": "col-sm-6 col-form-label col-form-label-sm",
                    "for": "y",
                    text: "Y"
                }),
                $("<div/>", {
                    "class": "col-sm-6",
                    html: $("<input/>", {
                        "class": "form-control form-control-sm",
                        "id": "y",
                        "placeholder": "Y value",
                        "name": "y",
                        value: point.getY(),
                        on: {
                            "change": function (e) {
                                var value = e.target.value;
                                point.setY(value);
                                e.stopPropagation();
                            }
                        }
                    })
                })
            ],
            on: {
                "shapeChange": function (e, data) {
                    var shape = e.data;
                    $(this).find("[name='x']").val(data.shape.getX());
                    $(this).find("[name='y']").val(data.shape.getY());
                    e.stopPropagation();
                }
            }
        });
    }
    function getPropertyFormGroup(name, value, disabled) {
        if (disabled === void 0) { disabled = false; }
        return $("<div/>", {
            "class": "row",
            html: [
                $("<label/>", {
                    "class": "col-sm-6 col-form-label col-form-label-sm",
                    "for": name,
                    text: name
                }),
                $("<div/>", {
                    "class": "col-sm-6",
                    html: $("<input/>", {
                        "class": "form-control form-control-sm",
                        "id": name,
                        "placeholder": name + " value",
                        "name": name,
                        value: value
                    }).prop('disabled', disabled)
                })
            ]
        });
    }
    function getCanvasContainer(__this) {
        return $("<div/>", {
            "class": (__this.options.toolbox) ? "col-sm-12 col-md-12 col-lg-8" : "col-sm-12",
            "height": __this.options.height,
            "width": __this.options.width,
            html: function () {
                var layers = [];
                __this.layers.map(function (layer, index) {
                    layers.push(layer.getCanvas());
                });
                return layers;
            }
        });
    }
    DOMObjectCreator.getCanvasContainer = getCanvasContainer;
    function getCanvas(id, zindex) {
        var canvas = $("<canvas/>", {
            "id": id,
            "class": "layer"
        }).css({
            "zIndex": zindex
        });
        return canvas;
    }
    DOMObjectCreator.getCanvas = getCanvas;
})(DOMObjectCreator || (DOMObjectCreator = {}));
var Geometry;
(function (Geometry) {
    var Layer = (function () {
        function Layer(options) {
            this.shapes = [];
            this.id = (options != undefined && options.id) ? options.id : new Date().getMilliseconds;
        }
        Layer.prototype.setId = function (_id) {
            this.id = _id;
        };
        Layer.prototype.getId = function () {
            return this.id;
        };
        Layer.prototype.getShapes = function () {
            return this.shapes;
        };
        Layer.prototype.setShapes = function (_shapes) {
            this.shapes = _shapes;
        };
        Layer.prototype.addShape = function (_shape) {
            this.shapes.push(_shape);
        };
        Layer.prototype.removeShape = function (_shape) {
        };
        Layer.prototype.removeAllShape = function () {
            this.shapes = [];
        };
        return Layer;
    }());
    Geometry.Layer = Layer;
})(Geometry || (Geometry = {}));
var Geometry;
(function (Geometry) {
    var Point = (function (_super) {
        __extends(Point, _super);
        function Point(_x, _y) {
            var _this = _super.call(this, "POINT") || this;
            _this.x = _x;
            _this.y = _y;
            return _this;
        }
        Point.prototype.getType = function () {
            return Point.className;
        };
        Point.prototype.getX = function () {
            return this.x;
        };
        Point.prototype.setX = function (_x) {
            this.x = _x;
            this.notifyChange("X");
        };
        Point.prototype.getY = function () {
            return this.y;
        };
        Point.prototype.setY = function (_y) {
            this.y = _y;
            this.notifyChange("Y");
        };
        Point.prototype.update = function (_timestamp) {
            var v = _super.prototype.update.call(this, _timestamp);
            this.move(v);
            return v;
        };
        Point.prototype.move = function (v) {
            this.x += v.getDx();
            this.y += v.getDy();
            this.notifyChange("XY");
        };
        Point.className = "Point";
        return Point;
    }(Geometry.Shape));
    Geometry.Point = Point;
})(Geometry || (Geometry = {}));
var Geometry;
(function (Geometry) {
    var Circle = (function (_super) {
        __extends(Circle, _super);
        function Circle(_center, _radius) {
            var _this = _super.call(this, "CIRCLE") || this;
            _this.center = _center;
            _this.radius = _radius;
            return _this;
        }
        Circle.prototype.getType = function () {
            return Circle.className;
        };
        Circle.prototype.getCenter = function () {
            return this.center;
        };
        Circle.prototype.setCenter = function (_center) {
            this.center = _center;
            this.center.notifyChange("center");
        };
        Circle.prototype.getRadius = function () {
            return this.radius;
            this.notifyChange("radius");
        };
        Circle.prototype.setRadius = function (_radius) {
            this.radius = _radius;
            this.notifyChange("radius");
        };
        Circle.prototype.update = function (_timestamp) {
            var v = _super.prototype.update.call(this, _timestamp);
            this.center.move(v);
            return v;
        };
        Circle.className = "Circle";
        return Circle;
    }(Geometry.Shape));
    Geometry.Circle = Circle;
})(Geometry || (Geometry = {}));
var Geometry;
(function (Geometry) {
    var Polygon = (function (_super) {
        __extends(Polygon, _super);
        function Polygon(_points, _color) {
            var _this = _super.call(this, "POLYGON", _color) || this;
            _this.points = (_points != null) ? _points : [];
            return _this;
        }
        ;
        Polygon.prototype.getType = function () {
            return Polygon.className;
        };
        Polygon.prototype.getPoints = function () {
            return this.points;
        };
        ;
        Polygon.prototype.setPoints = function (_points) {
            this.points = _points;
            this.notifyChange();
        };
        ;
        Polygon.prototype.addPoint = function (_point) {
            this.points.push(_point);
            this.notifyChange();
        };
        Polygon.prototype.update = function (_timestamp) {
            var v = _super.prototype.update.call(this, _timestamp);
            for (var _i = 0, _a = this.points; _i < _a.length; _i++) {
                var p = _a[_i];
                p.move(v);
            }
            return v;
        };
        Polygon.className = "Polygon";
        return Polygon;
    }(Geometry.Shape));
    Geometry.Polygon = Polygon;
})(Geometry || (Geometry = {}));
var Tools;
(function (Tools) {
    var Brush = (function () {
        function Brush(_ctx) {
            this.lineWidth = 1;
            this.fillStyle = "#FA0B16";
            this.strokeStyle = "#FA0B16";
            this.fill = true;
            if (_ctx != null)
                this.setCtx(_ctx);
        }
        Brush.prototype.getRatio = function () {
            return this.ctx.canvas.width / 1600;
        };
        Brush.prototype.drawShape = function (_shape) {
            this.setStrokeStyle(_shape.getColor());
            this.setFillStyle(_shape.getColor());
            if (_shape.isSelected()) {
                this.setStrokeStyle("red");
            }
            if (_shape instanceof Tools.ShapeCtrl)
                _shape = _shape.getShape();
            if (_shape instanceof Geometry.Polygon) {
                var pointArray = _shape.getPoints();
                var p0 = pointArray[pointArray.length - 1];
                this.ctx.beginPath();
                this.ctx.moveTo(p0.getX() * this.getRatio(), p0.getY() * this.getRatio());
                for (var i in _shape.getPoints()) {
                    var p = _shape.getPoints()[i];
                    this.ctx.lineTo(p.getX() * this.getRatio(), p.getY() * this.getRatio());
                }
                this.ctx.stroke();
            }
            else if (_shape instanceof Geometry.Circle) {
                var circle = _shape;
                this.ctx.beginPath();
                this.ctx.arc(circle.getCenter().getX() * this.getRatio(), circle.getCenter().getY() * this.getRatio(), circle.getRadius() * this.getRatio(), 0, 2 * Math.PI, false);
                this.ctx.fill();
                this.ctx.stroke();
            }
            else {
            }
        };
        Brush.prototype.getCtx = function () {
            return this.ctx;
        };
        Brush.prototype.setCtx = function (_ctx) {
            this.ctx = _ctx;
            this.ctx.fillStyle = this.fillStyle;
            this.ctx.lineWidth = this.lineWidth;
            this.ctx.strokeStyle = this.strokeStyle;
        };
        Brush.prototype.getLineWidth = function () {
            return this.lineWidth;
        };
        Brush.prototype.seLineWidth = function (_lineWidth) {
            this.lineWidth = _lineWidth;
            this.ctx.lineWidth = this.lineWidth;
        };
        Brush.prototype.getFillStyle = function () {
            return this.fillStyle;
        };
        Brush.prototype.setFillStyle = function (_fillStyle) {
            this.fillStyle = _fillStyle;
            this.ctx.fillStyle = this.fillStyle;
        };
        Brush.prototype.getStrokeStyle = function () {
            return this.strokeStyle;
        };
        Brush.prototype.setStrokeStyle = function (_strokeStyle) {
            this.strokeStyle = _strokeStyle;
            this.ctx.strokeStyle = this.strokeStyle;
        };
        return Brush;
    }());
    Tools.Brush = Brush;
})(Tools || (Tools = {}));
var Tools;
(function (Tools) {
    var LayerUI = (function () {
        function LayerUI(arg0, arg1) {
            var _this = this;
            this.ratio = 16 / 10;
            this._ratio = 10 / 16;
            this.dt = 20;
            this.selected = true;
            var options;
            if (arg0 instanceof Geometry.Layer) {
                this.layer = arg0;
                options = (arg1 == undefined) ? {} : arg1;
            }
            else {
                this.layer = new Geometry.Layer(arg0);
                options = arg0;
            }
            this.setShapes((options != undefined && options.shapes) ? options.shapes : []);
            this.canvas = (options.canvas) ? options.canvas : DOMObjectCreator.getCanvas();
            this.ctx = this.canvas[0].getContext("2d");
            this.brush = new Tools.Brush(this.ctx);
            $(this.canvas).ready(function (e) {
                _this.resize();
            });
            $(window).on('resize', function (e) {
                _this.resize();
            });
        }
        LayerUI.prototype.getId = function () {
            return this.layer.getId();
        };
        LayerUI.prototype.setId = function (_id) {
            this.layer.setId(_id);
        };
        LayerUI.prototype.getShapes = function () {
            return this.layer.getShapes();
        };
        LayerUI.prototype.setShapes = function (_shapes) {
            for (var _i = 0, _shapes_1 = _shapes; _i < _shapes_1.length; _i++) {
                var shape = _shapes_1[_i];
                this.addShape(shape);
            }
        };
        LayerUI.prototype.addShape = function (_shape) {
            this.layer.addShape(_shape);
            _shape.setParentLayer(this);
        };
        LayerUI.prototype.removeShape = function (_shape) {
            this.layer.removeShape(_shape);
        };
        LayerUI.prototype.removeAllShape = function () {
            this.layer.removeAllShape();
            this.notifyShapeChange();
        };
        LayerUI.prototype.setHeight = function (_height) {
            this.height = _height;
            this.canvas.attr("height", _height.toString());
        };
        LayerUI.prototype.getHeight = function () {
            return this.height;
        };
        LayerUI.prototype.setWidth = function (_width) {
            this.width = _width;
            this.canvas.attr("width", _width.toString());
        };
        LayerUI.prototype.getWidth = function () {
            return this.width;
        };
        LayerUI.prototype.getCanvas = function () {
            return this.canvas;
        };
        LayerUI.prototype.setCanvas = function (_canvas) {
            this.canvas = _canvas;
            this.ctx = this.canvas[0].getContext("2d");
            this.brush = new Tools.Brush(this.ctx);
        };
        LayerUI.prototype.getToolbox = function () {
            return this.toolbox;
        };
        LayerUI.prototype.setToolbox = function (_toolbox) {
            this.toolbox = _toolbox;
        };
        LayerUI.prototype.getBrush = function () {
            return this.brush;
        };
        LayerUI.prototype.seBrush = function (_brush) {
            this.brush = _brush;
        };
        LayerUI.prototype.toggleSelected = function () {
            this.selected = !this.selected;
            this.canvas.toggleClass("selected");
        };
        LayerUI.prototype.draw = function () {
            for (var _i = 0, _a = this.getShapes(); _i < _a.length; _i++) {
                var shape = _a[_i];
                this.brush.drawShape(shape);
            }
        };
        LayerUI.prototype.erase = function () {
            this.ctx.clearRect(0, 0, this.width, this.height);
        };
        LayerUI.prototype.render = function () {
            this.erase();
            this.draw();
        };
        LayerUI.prototype.update = function () {
            for (var _i = 0, _a = this.getShapes(); _i < _a.length; _i++) {
                var shape = _a[_i];
                shape.update(this.dt);
            }
        };
        LayerUI.prototype.startAnimation = function () {
            var __this = this;
            this.updateIntervalId = setInterval(function () {
                for (var _i = 0, _a = __this.getShapes(); _i < _a.length; _i++) {
                    var shape = _a[_i];
                    shape.update(__this.dt);
                }
            }, __this.dt);
        };
        LayerUI.prototype.stopAnimation = function () {
            clearInterval(this.updateIntervalId);
        };
        LayerUI.prototype.toggleAnimation = function () {
            (this.animated) ? this.stopAnimation() : this.startAnimation();
            this.animated = !this.animated;
        };
        LayerUI.prototype.startRendering = function () {
            var __this = this;
            function step(timestamp) {
                __this.render();
                __this.renderRequestID = requestAnimationFrame(step);
            }
            __this.renderRequestID = requestAnimationFrame(step);
        };
        LayerUI.prototype.stopRendering = function () {
            cancelAnimationFrame(this.renderRequestID);
        };
        LayerUI.prototype.toggleRendering = function () {
            (this.rendering) ? this.stopRendering() : this.startRendering();
            this.rendering = !this.rendering;
        };
        LayerUI.prototype.resize = function () {
            this.setWidth(this.canvas.parent().width());
            this.setHeight(this.canvas.parent().width() * this._ratio);
        };
        LayerUI.prototype.notifyShapeChange = function () {
            if (this.getToolbox() != undefined) {
                console.log("trigger 'notifyShapeChange'");
                this.getToolbox().trigger("notifyShapeChange");
            }
        };
        ;
        return LayerUI;
    }());
    Tools.LayerUI = LayerUI;
})(Tools || (Tools = {}));
var MP_Geometry;
(function (MP_Geometry) {
    var PaletteOptions = (function () {
        function PaletteOptions(args) {
            this.baseIndex = 1000;
            this.toolbox = false;
            this.layers = null;
        }
        return PaletteOptions;
    }());
    MP_Geometry.PaletteOptions = PaletteOptions;
    var Palette = (function () {
        function Palette(element, options) {
            this.layers = [];
            this.element = element;
            this.options = options;
            this.init();
        }
        Palette.prototype.init = function (_options) {
            var _this = this;
            if (_options != undefined) {
                this.options = $.extend(true, this.options, _options);
            }
            this.options.layers.map(function (layerOptions, index) {
                var l = new Tools.LayerUI(layerOptions);
                _this.layers.push(l);
            });
            var domObject = $("<div/>", {
                "class": "container-fluid",
                html: $("<div/>", {
                    "class": "row",
                    html: function () {
                        var res = [];
                        res.push(DOMObjectCreator.getCanvasContainer(_this));
                        if (_this.options.toolbox)
                            res.push(DOMObjectCreator.getToolBoxContainer(_this));
                        return res;
                    }
                })
            });
            $(this.element).data("mp_geometry", this)
                .addClass("mp_geometry")
                .html(domObject);
        };
        Palette.prototype.addLayer = function (_layers) {
            var _this = this;
            _layers.map(function (layerOptions, index) {
                var l = new Tools.LayerUI(layerOptions);
                _this.layers.push(l);
                _this.options.layers.push(layerOptions);
            });
        };
        Palette.prototype.getLayers = function (_options) {
            if (_options == undefined) {
                return this.layers;
            }
            else if (typeof _options == "string") {
                return _.find(this.layers, function (layer) { return _options == layer.getId(); });
            }
            else {
                return _.filter(this.layers, function (layer) { return _.includes(_options, layer.getId()); });
            }
        };
        Palette.prototype.getShapes = function (_options) {
            var getShapesFromLayers = function (_layers) {
                var shapes = [];
                for (var _i = 0, _layers_1 = _layers; _i < _layers_1.length; _i++) {
                    var layer = _layers_1[_i];
                    shapes = shapes.concat(layer.getShapes());
                }
                return shapes;
            };
            return getShapesFromLayers(this.getLayers(_options));
        };
        Palette.prototype.startRendering = function (_options) {
            for (var _i = 0, _a = this.getLayers(_options); _i < _a.length; _i++) {
                var l = _a[_i];
                l.startRendering();
            }
        };
        Palette.prototype.stopRendering = function (_options) {
            for (var _i = 0, _a = this.getLayers(_options); _i < _a.length; _i++) {
                var l = _a[_i];
                l.stopRendering();
            }
        };
        Palette.prototype.startAnimation = function (_options) {
            for (var _i = 0, _a = this.getLayers(_options); _i < _a.length; _i++) {
                var l = _a[_i];
                l.startAnimation();
            }
        };
        Palette.prototype.stopAnimation = function (_options) {
            for (var _i = 0, _a = this.getLayers(_options); _i < _a.length; _i++) {
                var l = _a[_i];
                l.stopAnimation();
            }
        };
        Palette.prototype.toolBox = function () {
            return;
        };
        return Palette;
    }());
    MP_Geometry.Palette = Palette;
})(MP_Geometry || (MP_Geometry = {}));
;
(function (w, $) {
    if (!$)
        return false;
    $.fn.extend({
        Palette: function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            if (args.length == 1 && typeof args[0] == "object") {
                var _options_1 = args[0];
                return this.each(function () {
                    var _element = $(this);
                    if (!_element.data("mp_geometry")) {
                        new MP_Geometry.Palette(_element, _options_1);
                    }
                    else {
                        var INSTANCE = _element.data('mp_geometry');
                        INSTANCE.init(_options_1);
                    }
                });
            }
            else if (typeof args[0] == "string") {
                var _element = $(this), _fn = args[0], _options = args[1], INSTANCE = _element.data('mp_geometry');
                try {
                    if (INSTANCE == undefined)
                        throw new Error("Not instance of MP_Geometry.Palette");
                }
                catch (e) {
                    console.error(e.name);
                    console.error(e.message);
                    return;
                }
                switch (_fn) {
                    case "getInstance":
                        return INSTANCE;
                    case "getLayers":
                        return INSTANCE.getLayers(_options);
                    case "getShapes":
                        return INSTANCE.getShapes(_options);
                    case "startRendering":
                        return INSTANCE.startRendering(_options);
                    case "stopRendering":
                        return INSTANCE.stopRendering(_options);
                    case "startAnimation":
                        return INSTANCE.startAnimation(_options);
                    case "stopAnimation":
                        return INSTANCE.stopAnimation(_options);
                    default:
                        break;
                }
                return;
            }
        }
    });
})(window, jQuery);
var Geometry;
(function (Geometry) {
    var GeometryFactory = (function () {
        function GeometryFactory() {
        }
        GeometryFactory.getInstance = function () {
            if (this.INSTANCE == null)
                this.INSTANCE = new GeometryFactory();
            return this.INSTANCE;
        };
        GeometryFactory.prototype._layerArray = function (options) {
            var res = [];
            for (var _i = 0, options_1 = options; _i < options_1.length; _i++) {
                var layerOptions = options_1[_i];
                res.push(this._layer(layerOptions));
            }
            return res;
        };
        GeometryFactory.prototype._layer = function (options) {
            var l = new Geometry.Layer();
            l.setId(options.id);
            l.setShapes(this._shapeArray(options.shapes));
            return l;
        };
        GeometryFactory.prototype._shapeArray = function (options) {
            var res = [];
            for (var _i = 0, options_2 = options; _i < options_2.length; _i++) {
                var shapeOptions = options_2[_i];
                res.push(this._shape(shapeOptions));
            }
            return res;
        };
        GeometryFactory.prototype._shape = function (options) {
            var s = null;
            switch (options.name) {
                case "POINT":
                    s = new Geometry.Point(options.x, options.y);
                    break;
                case "POLYGON":
                    var polygon = new Geometry.Polygon();
                    for (var _i = 0, _a = options.points; _i < _a.length; _i++) {
                        var p = _a[_i];
                        polygon.addPoint(new Geometry.Point(p.x, p.y));
                    }
                    s = polygon;
                    break;
                case "CIRCLE":
                    s = new Geometry.Circle(new Geometry.Point(options.center.x, options.center.y), options.radius);
                    break;
                default:
                    break;
            }
            return s;
        };
        GeometryFactory.INSTANCE = null;
        return GeometryFactory;
    }());
    Geometry.GeometryFactory = GeometryFactory;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=client.js.map