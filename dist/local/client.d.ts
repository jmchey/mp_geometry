/// <reference types="jquery" />
/// <reference types="jqueryui" />
declare namespace Geometry {
    class Vector {
        private name;
        private dx;
        private dy;
        constructor(_dx: number, _dy: number);
        getDx(): number;
        setDx(_dx: number): void;
        getDy(): number;
        setDy(_dy: number): void;
    }
}
declare namespace Geometry {
    interface ShapeInterface {
        getId(): any;
        getType(): string;
        setToolbox(toolbox: JQuery): void;
        getToolbox(): JQuery;
        setSelected(selected: boolean): void;
        isSelected(): boolean;
        setParentLayer(parentLayer: Tools.LayerUI): void;
        getParentLayer(): Tools.LayerUI;
        getName(): string;
        setName(_name: string): void;
        getColor(): string;
        setColor(_color: string): void;
        getSpin(): number;
        setSpin(_spin: number): void;
        getDirection(): number;
        setDirection(_direction: number): void;
        getSpeed(): number;
        setSpeed(_speed: number): void;
    }
}
declare namespace Geometry {
    abstract class Shape implements ShapeInterface {
        readonly id: any;
        private name;
        private color;
        private selected;
        private parentLayer;
        private toolbox;
        private spin;
        private direction;
        private speed;
        private static generateId;
        constructor(_name: string, _color?: string);
        abstract getType(): string;
        getId(): any;
        setToolbox(toolbox: JQuery): void;
        getToolbox(): JQuery;
        setSelected(selected: boolean): void;
        isSelected(): boolean;
        getParentLayer(): Tools.LayerUI;
        setParentLayer(parentLayer: Tools.LayerUI): void;
        getName(): string;
        setName(_name: string): void;
        getColor(): string;
        setColor(_color: string): void;
        getSpin(): number;
        setSpin(_spin: number): void;
        getDirection(): number;
        setDirection(_direction: number): void;
        getSpeed(): number;
        setSpeed(_speed: number): void;
        update(dt: number): Vector;
        notifyChange(value?: string): void;
    }
}
declare namespace Tools {
    class ShapeCtrl implements Geometry.ShapeInterface {
        protected shape: Geometry.Shape;
        protected leftCtrl: number;
        protected rightCtrl: number;
        protected leftPressed: boolean;
        protected rightPressed: boolean;
        protected _delta: number;
        constructor(_shape: Geometry.Shape);
        getShape(): Geometry.Shape;
        getType(): string;
        getId(): any;
        setToolbox(toolbox: JQuery): void;
        getToolbox(): JQuery;
        setSelected(selected: boolean): void;
        isSelected(): boolean;
        setParentLayer(parentLayer: Tools.LayerUI): void;
        getParentLayer(): Tools.LayerUI;
        getName(): string;
        setName(_name: string): void;
        getColor(): string;
        setColor(_color: string): void;
        getDirection(): number;
        setDirection(_direction: number): void;
        getSpeed(): number;
        setSpeed(_speed: number): void;
        getSpin(): number;
        setSpin(spin: number): void;
        getCenter(): Geometry.Point;
        getLeftCtrl(): any;
        setLeftCtrl(_leftCtrl: any): void;
        getRightCtrl(): any;
        setRightCtrl(_rightCtrl: any): void;
    }
}
declare namespace Tools {
    class ShapeCircularCtrlUI extends ShapeCtrl {
        constructor(_shape: Geometry.Shape);
        update(dt: number): Geometry.Vector;
    }
}
declare namespace Tools {
    class ShapeLinearCtrlUI extends ShapeCtrl {
        constructor(_shape: Geometry.Shape);
        update(dt: number): Geometry.Vector;
    }
}
declare namespace DOMObjectCreator {
    function getToolBoxContainer(__this: any): JQuery;
    function getCanvasContainer(__this: any): JQuery;
    function getCanvas(id?: string, zindex?: number): JQuery;
}
declare namespace Geometry {
    class Layer {
        protected id: any;
        protected shapes: Geometry.Shape[];
        constructor(options?: any);
        setId(_id: string): void;
        getId(): string;
        getShapes(): Geometry.Shape[];
        setShapes(_shapes: Geometry.Shape[]): void;
        addShape(_shape: Geometry.Shape): void;
        removeShape(_shape: Geometry.Shape): void;
        removeAllShape(): void;
    }
}
declare namespace Geometry {
    class Point extends Shape {
        static readonly className = "Point";
        private x;
        private y;
        constructor(_x: number, _y: number);
        getType(): string;
        getX(): number;
        setX(_x: number): void;
        getY(): number;
        setY(_y: number): void;
        update(_timestamp: number): Vector;
        move(v: Vector): void;
    }
}
declare namespace Geometry {
    class Circle extends Shape {
        static readonly className = "Circle";
        private radius;
        private center;
        constructor(_center?: Point, _radius?: number);
        getType(): string;
        getCenter(): Point;
        setCenter(_center: Point): void;
        getRadius(): number;
        setRadius(_radius: number): void;
        update(_timestamp: number): Vector;
    }
}
declare module Geometry {
    class Polygon extends Shape {
        static readonly className = "Polygon";
        private points;
        constructor(_points?: Point[], _color?: string);
        getType(): string;
        getPoints(): Point[];
        setPoints(_points: Point[]): void;
        addPoint(_point: Point): void;
        update(_timestamp: number): Vector;
    }
}
declare namespace Tools {
    class Brush {
        private ctx;
        private lineWidth;
        private fillStyle;
        private strokeStyle;
        private fill;
        constructor(_ctx?: Object);
        getRatio(): number;
        drawShape(_shape: Geometry.ShapeInterface): void;
        getCtx(): Object;
        setCtx(_ctx: Object): void;
        getLineWidth(): number;
        seLineWidth(_lineWidth: number): void;
        getFillStyle(): string;
        setFillStyle(_fillStyle: string): void;
        getStrokeStyle(): string;
        setStrokeStyle(_strokeStyle: string): void;
    }
}
declare namespace Tools {
    class LayerUI {
        private layer;
        private renderRequestID;
        private rendering;
        protected canvas: any;
        protected ctx: any;
        protected brush: Brush;
        protected ratio: number;
        protected _ratio: number;
        protected width: number;
        protected height: number;
        private updateIntervalId;
        private animated;
        private dt;
        private toolbox;
        protected selected: boolean;
        constructor(arg0: any, arg1?: any);
        getId(): string;
        setId(_id: string): void;
        getShapes(): Geometry.Shape[];
        setShapes(_shapes: Geometry.Shape[]): void;
        addShape(_shape: Geometry.Shape): void;
        removeShape(_shape: Geometry.Shape): void;
        removeAllShape(): void;
        setHeight(_height: number): void;
        getHeight(): number;
        setWidth(_width: number): void;
        getWidth(): number;
        getCanvas(): JQuery;
        setCanvas(_canvas: JQuery): void;
        getToolbox(): JQuery;
        setToolbox(_toolbox: JQuery): void;
        getBrush(): Brush;
        seBrush(_brush: Brush): void;
        toggleSelected(): void;
        draw(): void;
        erase(): void;
        render(): void;
        update(): void;
        startAnimation(): void;
        stopAnimation(): void;
        toggleAnimation(): void;
        startRendering(): void;
        stopRendering(): void;
        toggleRendering(): void;
        resize(): void;
        notifyShapeChange(): void;
    }
}
declare module MP_Geometry {
    interface IPaletteOptions {
        baseIndex: number;
        toolbox: boolean;
        layers: Object[];
    }
    class PaletteOptions implements IPaletteOptions {
        baseIndex: number;
        toolbox: boolean;
        layers: Object[];
        constructor(args: any);
    }
    class Palette {
        element: JQuery;
        options: PaletteOptions;
        instance: string;
        layers: Tools.LayerUI[];
        constructor(element: JQuery, options: PaletteOptions);
        init(_options?: PaletteOptions): void;
        addLayer(_layers: any[]): void;
        getLayers(_options: any): any;
        getShapes(_options: any): any;
        startRendering(_options: any): any;
        stopRendering(_options: any): any;
        startAnimation(_options: any): any;
        stopAnimation(_options: any): any;
        toolBox(): JQuery;
    }
}
declare namespace Geometry {
    class GeometryFactory {
        private static INSTANCE;
        private constructor();
        static getInstance(): GeometryFactory;
        _layerArray(options: any[]): Layer[];
        _layer(options: any): Layer;
        _shapeArray(options: any[]): Shape[];
        _shape(options: any): Shape;
    }
}
