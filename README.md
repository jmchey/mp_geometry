# mp_geometry -Palette widget (https://bitbucket.org/jmchey/mp_geometry#readme)

##Introduction

I developed this small librairy to get to know TypeScript / NodeJS / NPM... I wanted at that time to make a small real time game in node JS and I wanted to see if I would be able to code the graphic interface myself.

Here is "MP_Geometry" a small librairy that allow you to display animate and control simple shapes on an HTML canvas

## Table of contents
* [Install](#install)
* [Usage](#usage)
* [Dependencies](#dependencies)
* [Fork me](#fork-me)

## Install with npm
Run command
```sh
npm install mp_geometry
```

## Usage

###Init

```html
	<!-- widget holder -->
	<div id="sample"></div>
```

```jquery
	$("#sample").Palette(<options>);
```

###Options
See below a list of available options (01/03/2019). Tired of reading ? Have a look on the examples below.

**layers**  
Description : 	List of the layers. Each layer can display and animate shapes.  
Type : 			array of LayerUI  
Optional  
Properties 

**toolbox**   
Description :	Boolean that indicate if the toolbox is displayed. The toolbox allow you to interact with the "palette" widget. Render, erase, animate layers...   
Type : boolean   
Optional (default : false )

###Methods  
How to call methods on the JQuery widget ?
```js
$("#sample").Palette(<method>,<options>)
```

**Get Layers getLayers(<options>)**  
Description : This method return an Array of LayerUI objects. You can specify the Layer IDs you want to get by passing them as parameters.  
options :  
* default : null   
* string : layerId   
* string[] : [layerId, ...]


**Example**  
```js
	$("#sample").Palette("getLayers")
	// Or specify IDs
	$("#sample").Palette("getLayers", ["MyLayerID1", "MyLayerID2" ])
```	

**Set Layers** setLayers(<options>)


## Tests

```sh
npm install
npm test
```

## Dependencies


##Fork Me

###Compile typescript
Run command
```
	tsc
```

